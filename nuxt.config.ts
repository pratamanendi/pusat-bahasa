// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    ssr: true,
    modules: [
        '@nuxtjs/tailwindcss',
        '@vueuse/nuxt',
    ],
    app: {
        head: {
            title: 'Website Pusat Bahasa IIK Bhakti Wiyata',
            meta: [
                { charset: 'utf-8' },
                { name: 'viewport', content: 'width=device-width, initial-scale=1' },
                { hid: 'description', name: 'description', content: 'Website Resmi Pusat Bahasa IIK Bhakti Wiyata' },
            ],
            link: [
                // { rel: 'icon', type: 'image/x-icon', href: '/img/asset-10.webp' },
                { rel: 'icon', type: 'image/x-icon', href: '/pusat-bahasa/img/asset-10.webp' }, // for generate
                { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
                { rel: 'preconnect', href: 'https://fonts.gstatic.com' },
                { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap' }
            ]
        },
        pageTransition: { name: 'page', mode: 'out-in' },
        layoutTransition: { name: 'layout', mode: 'out-in' },
        baseURL: '/pusat-bahasa/'
    },

})
