import { p as publicAssetsURL } from './renderer.mjs';
import { mergeProps, useSSRContext } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/index.mjs';
import { ssrRenderAttrs, ssrRenderAttr } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/server-renderer/index.mjs';
import { _ as _imports_0, a as _imports_2 } from './asset-24-4d3533db.mjs';
import { _ as _export_sfc } from './server.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-bundle-renderer@1.0.2/node_modules/vue-bundle-renderer/dist/runtime.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/h3@1.6.2/node_modules/h3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ufo@1.1.1/node_modules/ufo/dist/index.mjs';
import './nitro-prerenderer.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/node-fetch-native@1.0.2/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ofetch@1.0.1/node_modules/ofetch/dist/node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/destr@1.2.2/node_modules/destr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unenv@1.2.2/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/hookable@5.5.1/node_modules/hookable/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/defu@6.1.2/node_modules/defu/dist/defu.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ohash@1.0.0/node_modules/ohash/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/drivers/fs.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/radix3@1.0.0/node_modules/radix3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unctx@2.1.2/node_modules/unctx/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+ssr@1.1.23/node_modules/@unhead/ssr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unhead@1.1.23/node_modules/unhead/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+shared@1.1.23/node_modules/@unhead/shared/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-router@4.1.6_vue@3.2.47/node_modules/vue-router/dist/vue-router.node.mjs';

const _imports_1 = "" + publicAssetsURL("img/asset-30.webp");
const _imports_3 = "" + publicAssetsURL("img/asset-28.webp");
const _sfc_main = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "text-sm lg:min-h-[40vh]" }, _attrs))}><h3 class="font-bold text-base mb-3">Pendaftaran TOEIC</h3><p class="mb-8"> Pendaftaran TOEIC dibuka setidaknya dua kali dalam satu tahun akademik yaitu terjadwal di bulan Januari/Februari dan Juli/Agustus. Informasi tentang pendaftaran TOEIC ini akan dikeluarkan melalui surat edaran pemberitahuan TOEIC oleh Direktur Akademik ke fakultas dan prodi melalui e-letter. Mahasiswa melakukan pendaftaran TOEIC melalui OASIS selambat-lambatnya 2 minggu sebelum pelaksanaan tes dengan tahapan sebagai berikut. </p><div class="flex gap-8 text-sm"><div class="lg:w-1/5 md:w-1/3 space-y-2 md:block hidden"><img${ssrRenderAttr("src", _imports_0)} alt="Step 1 : pendaftaran toeic internal iik bhakta"><img${ssrRenderAttr("src", _imports_1)} alt="Step 2 : pendaftaran toeic internal iik bhakta"><img${ssrRenderAttr("src", _imports_2)} alt="Step 3 : pendaftaran toeic internal iik bhakta"><img${ssrRenderAttr("src", _imports_3)} alt="Step 4 : pendaftaran toeic internal iik bhakta"></div><div class="md:w-2/3 lg:w-4/5 px-3"><ol class="list-decimal list-outside space-y-2"><li>Masuk OASIS di menu Pendaftaran Tes Kelas Bahasa.</li><li>Pilih jenis tes yang akan diikuti, klik TOEIC.</li><li> Tahun Ajaran diisi sesuai dengan tahun akademik yang sedang ber langsung. Tanggal, jam dan sesi tes diisi sesuai dengan jadwal tes yang dikehendaki. Kolom note wajib diisi nomor Whatsapp aktif peserta tes. Klik Simpan. </li><li>Pembayaran TOEIC dilakukan oleh mahasiswa melalui <a href="https://iik.ac.id/payment" class="font-medium">https://iik.ac.id/payment</a> , dengan memasukkan kode tagihan yang muncul setelah mahasiswa melakukan pendaftaran TOEIC di OASIS seperti pada tampilan berikut. </li><li> Apabila mahasiswa sudah membayar TOEIC maka status pembayarannya akan menunjukkan sudah bayar dan selanjutnya mahasiswa bisa mengunggah bukti pembayaran tes ke OASIS di menu upload bukti. </li></ol></div></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/programs/program-toeic/internal.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const internal = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { internal as default };
//# sourceMappingURL=internal-9cee5148.mjs.map
