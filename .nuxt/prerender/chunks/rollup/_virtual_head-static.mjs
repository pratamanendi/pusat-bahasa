const _virtual__headStatic = {"headTags":"<meta charset=\"utf-8\">\n<title>Website Pusat Bahasa IIK Bhakti Wiyata</title>\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n<meta name=\"description\" content=\"Website Resmi Pusat Bahasa IIK Bhakti Wiyata\">\n<link rel=\"icon\" type=\"image/x-icon\" href=\"/pusat-bahasa/img/asset-10.webp\">\n<link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\n<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\">\n<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap\">","bodyTags":"","bodyTagsOpen":"","htmlAttrs":"","bodyAttrs":""};

export { _virtual__headStatic as default };
//# sourceMappingURL=_virtual_head-static.mjs.map
