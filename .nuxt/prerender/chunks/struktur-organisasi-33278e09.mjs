import { mergeProps, useSSRContext } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/index.mjs';
import { ssrRenderAttrs, ssrRenderList, ssrRenderAttr, ssrRenderStyle, ssrInterpolate } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/server-renderer/index.mjs';
import { u as useHead } from './server.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ofetch@1.0.1/node_modules/ofetch/dist/node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/hookable@5.5.1/node_modules/hookable/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unctx@2.1.2/node_modules/unctx/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+ssr@1.1.23/node_modules/@unhead/ssr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unhead@1.1.23/node_modules/unhead/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+shared@1.1.23/node_modules/@unhead/shared/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-router@4.1.6_vue@3.2.47/node_modules/vue-router/dist/vue-router.node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/h3@1.6.2/node_modules/h3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ufo@1.1.1/node_modules/ufo/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/defu@6.1.2/node_modules/defu/dist/defu.mjs';
import './nitro-prerenderer.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/node-fetch-native@1.0.2/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/destr@1.2.2/node_modules/destr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unenv@1.2.2/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ohash@1.0.0/node_modules/ohash/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/drivers/fs.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/radix3@1.0.0/node_modules/radix3/dist/index.mjs';

const _sfc_main = {
  __name: "struktur-organisasi",
  __ssrInlineRender: true,
  setup(__props) {
    useHead({
      title: "Struktur Organisasi Pusat Bahasa IIK Bhakti Wiyata",
      meta: [
        { name: "description", content: "Struktur organisasi kami" }
      ]
    });
    const organisasis = [
      {
        name: "Yeni Nurmala H., S.Pd., M.Pd.",
        title: "Kepala Pusat Bahasa",
        nik: "2016.0816",
        img: "org-1"
      },
      {
        name: "Atik Ulinuha, S.Pd., M.Pd.",
        title: "staf pengajar bahasa inggris",
        nik: "2008.0230",
        img: "org-2"
      },
      {
        name: "Widji Lodharta",
        title: "staf pengajar bahasa mandarin",
        nik: "2018.0963",
        img: "org-3"
      },
      {
        name: "Novita Sari A., S.Pd.",
        title: "Staf pengajar bahasa mandarin",
        nik: "2016.0816",
        img: "org-4"
      },
      {
        name: "Yuliatin Hariyati S.Pd.",
        title: "staf pengajar bahasa mandarin",
        nik: "2019.1042",
        img: "org-5"
      },
      {
        name: "Aryief Hidayatullah, B.ed.",
        title: "staf pengajar bahasa mandarin",
        nik: "2019.5041",
        img: "org-6"
      },
      {
        name: "Nandya Putri Pratama, S.S.",
        title: "staf administrasi",
        nik: "2021.1161",
        img: "org-7"
      },
      {
        name: "Dra. Hilarius Semana, M.Pd.",
        title: "staf pengajar bahasa inggris",
        subtitle: "(Part-time)",
        nik: "1989.0019",
        img: "org-8"
      },
      {
        name: "Palupi Susilowati, S.S., M.Pd.",
        title: "staf pengajar bahasa inggris",
        subtitle: "(Part-time)",
        nik: "2009.0327",
        img: "org-9"
      }
    ];
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "w-full lg:min-h-screen bg-slate-200 bg-gradient-to-tr from-red-400/30 to-indigo-400/30" }, _attrs))}><div class="lg:px-28 lg:py-14 p-6"><h1 class="uppercase lg:text-4xl text-xl font-bold tracking-wide lg:mb-0 mb-6">struktur organisasi</h1><div class="grid lg:grid-cols-4 md:grid-cols-3 grid-cols-2 md:gap-4 gap-2 md:my-8"><!--[-->`);
      ssrRenderList(organisasis, (org) => {
        _push(`<div><div class="card lg:grid lg:place-items-center space-y-3"><img${ssrRenderAttr("src", `/pusat-bahasa/img/${org.img}.webp`)} class="md:h-[250px] h-[180px] mx-auto mt-4 md:mt-0" loading="lazy"><div class="text-center md:text-sm text-xs" style="${ssrRenderStyle({ "user-select": "none" })}"><p class="font-bold capitalize">${ssrInterpolate(org.title)}</p><p class="">${ssrInterpolate(org.name)}</p>`);
        if (org.subtitle) {
          _push(`<p>${ssrInterpolate(org.subtitle)}</p>`);
        } else {
          _push(`<!---->`);
        }
        _push(`<p class="">NIK ${ssrInterpolate(org.nik)}</p></div></div></div>`);
      });
      _push(`<!--]--></div></div></div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/profile/struktur-organisasi.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=struktur-organisasi-33278e09.mjs.map
