import { p as publicAssetsURL } from './renderer.mjs';
import { mergeProps, useSSRContext } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/index.mjs';
import { ssrRenderAttrs, ssrRenderAttr, ssrRenderStyle } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/server-renderer/index.mjs';
import { _ as _imports_0$1 } from './asset-3-76052890.mjs';
import { _ as _export_sfc } from './server.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-bundle-renderer@1.0.2/node_modules/vue-bundle-renderer/dist/runtime.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/h3@1.6.2/node_modules/h3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ufo@1.1.1/node_modules/ufo/dist/index.mjs';
import './nitro-prerenderer.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/node-fetch-native@1.0.2/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ofetch@1.0.1/node_modules/ofetch/dist/node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/destr@1.2.2/node_modules/destr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unenv@1.2.2/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/hookable@5.5.1/node_modules/hookable/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/defu@6.1.2/node_modules/defu/dist/defu.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ohash@1.0.0/node_modules/ohash/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/drivers/fs.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/radix3@1.0.0/node_modules/radix3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unctx@2.1.2/node_modules/unctx/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+ssr@1.1.23/node_modules/@unhead/ssr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unhead@1.1.23/node_modules/unhead/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+shared@1.1.23/node_modules/@unhead/shared/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-router@4.1.6_vue@3.2.47/node_modules/vue-router/dist/vue-router.node.mjs';

const _imports_0 = "" + publicAssetsURL("img/asset-1.webp");
const _imports_2 = "" + publicAssetsURL("img/asset-2.webp");
const _sfc_main = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "lg:min-h-screen w-full" }, _attrs))}><div class="relative"><img${ssrRenderAttr("src", _imports_0)} alt="punggawa pusat bahasa iik bhakta" class="w-full" loading="lazy"><h1 style="${ssrRenderStyle({ "user-select": "none" })}" class="absolute grid w-full text-center px-4 md:px-0 top-[10%] left-1/2 -translate-x-1/2 -space-y-2"><span class="lg:text-4xl md:text-2xl text-sm font-sans font-semibold">Welcome to</span><span class="lg:text-6xl sm:text-xl md:text-3xl font-bold text-[rgb(13,70,125)]">Pusat Bahasa</span><span class="font-serif lg:text-5xl sm:text-2xl md:text-xl text-lg text-[rgb(13,70,125)]"><em>Bhakti Wiyata</em></span></h1></div><div class="lg:flex lg:w-4/5 justify-between items-center mx-auto lg:py-12 py-4 lg:px-0 px-4"><div class="lg:w-1/2 w-full md:mb-4 mb-2"><img${ssrRenderAttr("src", _imports_0$1)} class="w-1/3 md:w-1/4 lg:w-2/3 mx-auto" alt="Pusat Bahasa IIK Bhakta" loading="lazy"></div><div class="lg:w-3/4 lg:text-base text-sm space-y-3 text-justify"><p>Lahirnya Pusat Bahasa IIK Bhakta Kediri diawali dengan lahirnya program khusus kelas bahasa Inggris dan Mandarin bagi mahasiswa sebagai pilot projects pada tahun 2018. Pentingnya penguasaan bahasa asing agar bisa bersaing secara global menjadikan dasar IK Bhakta Kediri untuk mendirikan Pusat Bahasa sebagai salah satu unit penunjang yang menyediakan layanan bahasa bagi sivitas akademika untuk meningkatkan kemampuan berbahasa asing yaitu bahasa Inggris dan Mandarin. </p><p> Sejak tahun 2018, Pusat Bahasa menjalankan program kelas bahasa Inggris dan Mandarin bagi mahasiswa untuk mendapatkan sertifikat yang bisa digunakan sebagai syarat wisuda, nilai tambah pada CV (Curriculum Vitae) untuk melamar kerja, hingga syarat mengajukan beasiswa maupun studi lanjut. Sampai dengan saat ini, Pusat Bahasa IIK Bhakta membawahi dua pelayanan pembelajaran bahasa yaitu bahasa Inggris dan bahasa Mandarin. </p></div></div><div class="hidden md:block"><img${ssrRenderAttr("src", _imports_2)} alt="sanksi penyalahgunaan logo toeic &amp; ets" srcset="" loading="lazy"></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const index = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { index as default };
//# sourceMappingURL=index-f1976b4f.mjs.map
