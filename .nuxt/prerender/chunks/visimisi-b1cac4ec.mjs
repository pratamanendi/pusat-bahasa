import { p as publicAssetsURL } from './renderer.mjs';
import { mergeProps, useSSRContext } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/index.mjs';
import { ssrRenderAttrs, ssrRenderAttr } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/server-renderer/index.mjs';
import { _ as _export_sfc, u as useHead } from './server.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-bundle-renderer@1.0.2/node_modules/vue-bundle-renderer/dist/runtime.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/h3@1.6.2/node_modules/h3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ufo@1.1.1/node_modules/ufo/dist/index.mjs';
import './nitro-prerenderer.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/node-fetch-native@1.0.2/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ofetch@1.0.1/node_modules/ofetch/dist/node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/destr@1.2.2/node_modules/destr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unenv@1.2.2/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/hookable@5.5.1/node_modules/hookable/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/defu@6.1.2/node_modules/defu/dist/defu.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ohash@1.0.0/node_modules/ohash/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/drivers/fs.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/radix3@1.0.0/node_modules/radix3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unctx@2.1.2/node_modules/unctx/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+ssr@1.1.23/node_modules/@unhead/ssr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unhead@1.1.23/node_modules/unhead/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+shared@1.1.23/node_modules/@unhead/shared/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-router@4.1.6_vue@3.2.47/node_modules/vue-router/dist/vue-router.node.mjs';

const _imports_0 = "" + publicAssetsURL("img/asset-11.webp");
const _sfc_main = {
  __name: "visimisi",
  __ssrInlineRender: true,
  setup(__props) {
    useHead({
      title: "Visi Misi Pusat Bahasa IIK Bhakti Wiyata",
      meta: [
        { name: "description", content: "Visi dan misi pusat bahasa iik bhakti wiyata" }
      ]
    });
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "w-full lg:min-h-screen bg-slate-200 bg-gradient-to-tr from-red-400/30 to-indigo-400/30" }, _attrs))} data-v-8d371633><div class="lg:px-28 md:px-14 lg:py-14 md:py-14 p-6" data-v-8d371633><h1 class="uppercase text-4xl font-bold tracking-wide" data-v-8d371633>visi misi</h1><div class="flex lg:flex-row flex-col-reverse items-center md:items-start gap-2" data-v-8d371633><div class="lg:w-1/2 space-y-6 py-8 lg:text-sm xl:text-base" data-v-8d371633><div class="" data-v-8d371633><p class="font-bold uppercase" data-v-8d371633>visi</p><p data-v-8d371633>Menjadi penyedia layanan bahasa yang kompetitif dalam rangka meningkatkan kualitas sumber daya manusia yang unggul dan mampu berperan aktif dalam pembangunan bangsa.</p></div><div class="" data-v-8d371633><p class="font-bold uppercase" data-v-8d371633>misi</p><ol class="list-decimal list-outside" data-v-8d371633><li data-v-8d371633>Menyelenggarakan pembelajaran bahasa asing (Inggris dan Mandarin) bagi civitas akademika IIK Bhakta Kediri.</li><li data-v-8d371633>Memberikan pelatihan bahasa asing (Inggris dan Mandarin) kepada civitas akadermika IIK Bhakta Kediri.</li><li data-v-8d371633>Memberikan pelayanan penerjemahan bahasa asing kepada civitas akademika IIK Bhakta Kediri.</li><li data-v-8d371633>Menjalinan kerjasama antar lembaga di luar IIK Bhakta guna meningkatkan mutu dan layanan.</li></ol></div><div class="" data-v-8d371633><p class="font-bold uppercase" data-v-8d371633>program</p><ol class="list-decimal list-outside" data-v-8d371633><li data-v-8d371633> Menyelenggarakan pembelajaran bahasa Inggris di program kelas bahasa Inggris persiapan TOEIC bagi seluruh mahasiswa IIK Bhakta Kediri dalam satu level/tahap. </li><li data-v-8d371633> Menyelenggarakan pembelajaran bahasa Mandarin di program kelas bahasa Mandarin bagi seluruh mahasiswa IIK Bhakta Kediri dalam dua level. </li><li data-v-8d371633> Menyelenggarakan tes English for Mapping bekerjasama dengan International Test Centre (ITC) bagi mahasiswa baru sebagai prasyarat mengikuti kelas bahasa Inggris persiapan TOEIC. </li><li data-v-8d371633> Menyelenggarakan TOEIC bekerjasama dengan International Test Centre (ITC) bagi mahasiswa yang telah menyelesaikan kelas Bahasa Inggris guna mendapatkan sertifikat TOEIC sebagai salah satu syarat wisuda. </li><li data-v-8d371633> Menyelenggarakan TOEIC bagi dosen IIK Bhakta Kediri dan masyakarat umum. </li><li data-v-8d371633> Menyelenggarakan tes Bahasa Mandarin bagi mahasiswa IIK Bhakta Kediri </li><li data-v-8d371633> Mengeluarkan sertifikat bahasa Mandarin bagi mahasiswa yang sudah lulus kelas bahasa Mandarin. </li><li data-v-8d371633> Memberikan layanan penerjemah/alih bahasa Inggris dan Mandarin bagi civitas akadernika IIK Bhakta Kediri. </li></ol></div></div><div class="lg:w-1/2 md:w-2/3 w-5/6 self-center lg:self-start" data-v-8d371633><img${ssrRenderAttr("src", _imports_0)} class="" alt="visi dan misi pusat bahasa iik bhakti wiyata" data-v-8d371633></div></div></div></div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/profile/visimisi.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const visimisi = /* @__PURE__ */ _export_sfc(_sfc_main, [["__scopeId", "data-v-8d371633"]]);

export { visimisi as default };
//# sourceMappingURL=visimisi-b1cac4ec.mjs.map
