import { mergeProps, useSSRContext } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/index.mjs';
import { ssrRenderAttrs } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/server-renderer/index.mjs';
import { _ as _export_sfc } from './server.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ofetch@1.0.1/node_modules/ofetch/dist/node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/hookable@5.5.1/node_modules/hookable/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unctx@2.1.2/node_modules/unctx/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+ssr@1.1.23/node_modules/@unhead/ssr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unhead@1.1.23/node_modules/unhead/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+shared@1.1.23/node_modules/@unhead/shared/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-router@4.1.6_vue@3.2.47/node_modules/vue-router/dist/vue-router.node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/h3@1.6.2/node_modules/h3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ufo@1.1.1/node_modules/ufo/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/defu@6.1.2/node_modules/defu/dist/defu.mjs';
import './nitro-prerenderer.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/node-fetch-native@1.0.2/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/destr@1.2.2/node_modules/destr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unenv@1.2.2/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ohash@1.0.0/node_modules/ohash/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/drivers/fs.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/radix3@1.0.0/node_modules/radix3/dist/index.mjs';

const _sfc_main = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "space-y-3 lg:min-h-[40vh] text-sm" }, _attrs))} data-v-ccb52481><h1 class="font-bold capitalize text-base" data-v-ccb52481>TOEIC Public</h1><p data-v-ccb52481>IIK Bhakta menjadi tempat penyelenggara tes TOEIC yang diselenggarakan oleh ITC. Tes TOEIC sangat diperlukan sebagai syarat pengajuan beasiswa, melamar pekerjaan dan keperluan lainya. Adapun jadwal pelaksanaanya dapat disimak berikut:</p><p class="font-bold" data-v-ccb52481>Jadwal TOEIC Listening &amp; Reading Public Test</p><p class="font-bold" data-v-ccb52481>Hari kerja: 09.00 - 11.30 (sesuai waktu setempat):</p><div class="bg-[#0d467d] px-4 md:min-w-fit w-3/4 py-2 rounded-xl shadow-sm my-3" data-v-ccb52481><div id="date" class="grid md:grid-cols-3 text-white divide-y divide-slate-100" data-v-ccb52481><p data-v-ccb52481>24 Januari 2023</p><p data-v-ccb52481>7 Februari 2023</p><p data-v-ccb52481>21 Maret 2023</p><p data-v-ccb52481>18 April 2023</p><p data-v-ccb52481>23 Mei 2023</p><p data-v-ccb52481>6 Juni 2023</p><p data-v-ccb52481>25 Juli 2023</p><p data-v-ccb52481>8 Agustus 2023</p><p data-v-ccb52481>26 September 2023</p><p data-v-ccb52481>10 Oktober 2023</p><p data-v-ccb52481>21 November 2023</p><p data-v-ccb52481>19 Desember 2023</p></div></div><p class="font-bold" data-v-ccb52481>Ketentuan tes:</p><ol class="!mt-0 list-outside list-decimal" data-v-ccb52481><li data-v-ccb52481>Peserta daat langsung mendaftar melalui website resmi ITC <a href="https://smartcart.id/register" class="text-sky-600" data-v-ccb52481>https://smartcart.id/register</a></li><li data-v-ccb52481>Biaya tes Rp. 675.000 dan langsung dibayarkan pada pihak ITC atau membayar melalui marketplace Tokopedia <a href="https://www.tokopedia.com/belajar/" class="text-sky-600" data-v-ccb52481>https://www.tokopedia.com/belajar/</a></li><li data-v-ccb52481>Tidak ada minimal peserta</li></ol></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/programs/program-toeic/umum.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const umum = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender], ["__scopeId", "data-v-ccb52481"]]);

export { umum as default };
//# sourceMappingURL=umum-3e19656a.mjs.map
