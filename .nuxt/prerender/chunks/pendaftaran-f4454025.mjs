import { p as publicAssetsURL } from './renderer.mjs';
import { mergeProps, useSSRContext } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/index.mjs';
import { ssrRenderAttrs, ssrRenderAttr } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/server-renderer/index.mjs';
import { _ as _imports_0, a as _imports_2 } from './asset-24-4d3533db.mjs';
import { _ as _export_sfc } from './server.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-bundle-renderer@1.0.2/node_modules/vue-bundle-renderer/dist/runtime.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/h3@1.6.2/node_modules/h3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ufo@1.1.1/node_modules/ufo/dist/index.mjs';
import './nitro-prerenderer.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/node-fetch-native@1.0.2/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ofetch@1.0.1/node_modules/ofetch/dist/node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/destr@1.2.2/node_modules/destr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unenv@1.2.2/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/hookable@5.5.1/node_modules/hookable/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/defu@6.1.2/node_modules/defu/dist/defu.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ohash@1.0.0/node_modules/ohash/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/drivers/fs.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/radix3@1.0.0/node_modules/radix3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unctx@2.1.2/node_modules/unctx/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+ssr@1.1.23/node_modules/@unhead/ssr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unhead@1.1.23/node_modules/unhead/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+shared@1.1.23/node_modules/@unhead/shared/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-router@4.1.6_vue@3.2.47/node_modules/vue-router/dist/vue-router.node.mjs';

const _imports_1 = "" + publicAssetsURL("img/asset-33.webp");
const _sfc_main = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "space-y-3 lg:min-h-[40vh] text-sm" }, _attrs))}><h1 class="font-bold capitalize text-base">Pendaftaran tes English for Mapping</h1><p class="text-justify text-sm">Pendaftaran tes English for Mapping dibuka setidaknya dua kali dalam satu tahun akademik yaitu setiap sebelum semester gasal atau genap dimulai, yaitu di bulan Februari dan Agustus. Informasi tentang pendaftaran English for Mapping ini akan dikeluarkan oleh Direktur Akademik melalui surat edaran pemberitahuan English for Mapping ke fakultas dan prodi melalui e-letter. Mahasiswa melakukan pendaftaran English for Mapping melalui OASIS selambat-lambatnya 2 minggu sebelum pelaksanaan tes dengan tahapan sebagai berikut. </p><div class="space-y-3 px-4 text-sm"><div class="flex md:gap-6 gap-2"><img${ssrRenderAttr("src", _imports_0)} class="w-1/4 md:w-1/5" alt=""><p class="md:w-4/5 w-3/4">1. Masuk OASIS di menu Pendaftaran Tes Kelas Bahasa.</p></div><div class="flex md:gap-6 gap-2"><img${ssrRenderAttr("src", _imports_1)} class="w-1/4 md:w-1/5" alt=""><p class="md:w-4/5 w-3/4">2. Pilih jenis tes yang akan diikuti, klik English for Mapping.</p></div><div class="flex md:gap-6 gap-2"><img${ssrRenderAttr("src", _imports_2)} class="w-1/4 h-full md:w-1/5" alt=""><p class="md:w-4/5 w-3/4">3. Tahun ajaran diisi sesuai dengan tahun akademik yang sedang berlangsung. Tanggal, jam dan sesi tes diisi sesuai dengan jadwal tes yang dikehendaki. kolom note wajib diisi nomor Whatsapp aktif peserta tes. Klik Simpan.</p></div></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/programs/english-for-mapping/pendaftaran.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const pendaftaran = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { pendaftaran as default };
//# sourceMappingURL=pendaftaran-f4454025.mjs.map
