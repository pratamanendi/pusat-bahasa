import { p as publicAssetsURL } from './renderer.mjs';
import { mergeProps, useSSRContext } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/index.mjs';
import { ssrRenderAttrs, ssrRenderAttr } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/server-renderer/index.mjs';
import { _ as _imports_0, a as _imports_2 } from './asset-24-4d3533db.mjs';
import { _ as _export_sfc } from './server.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-bundle-renderer@1.0.2/node_modules/vue-bundle-renderer/dist/runtime.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/h3@1.6.2/node_modules/h3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ufo@1.1.1/node_modules/ufo/dist/index.mjs';
import './nitro-prerenderer.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/node-fetch-native@1.0.2/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ofetch@1.0.1/node_modules/ofetch/dist/node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/destr@1.2.2/node_modules/destr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unenv@1.2.2/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/hookable@5.5.1/node_modules/hookable/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/defu@6.1.2/node_modules/defu/dist/defu.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ohash@1.0.0/node_modules/ohash/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/drivers/fs.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/radix3@1.0.0/node_modules/radix3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unctx@2.1.2/node_modules/unctx/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+ssr@1.1.23/node_modules/@unhead/ssr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unhead@1.1.23/node_modules/unhead/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+shared@1.1.23/node_modules/@unhead/shared/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-router@4.1.6_vue@3.2.47/node_modules/vue-router/dist/vue-router.node.mjs';

const _imports_1 = "" + publicAssetsURL("img/asset-23.webp");
const _sfc_main = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "text-sm lg:min-h-[40vh]" }, _attrs))}><h3 class="font-bold mb-3 capitalize text-base">Pendaftaran Kelas Bahasa Mandarin</h3><p class="mb-8"> Pendaftaran kelas bahasa Mandarin dibuka setiap awal semester, yaitu di minggu pertama dan kedua perkuliahan regular dimulai sesuai kalender akademik. Mahasiswa yang telah menyelesaikan administrasi perkuliahan termasuk melakukan KRS (Kartu Rencana Studi) bisa melakukan pendaftaran secara online melalui OASIS dengan tahapan sebagai berikut. </p><div class="flex gap-8 text-sm lg:min-h-[40vh]"><div class="lg:w-1/5 md:w-1/3 space-y-6 py-4 md:block hidden"><img${ssrRenderAttr("src", _imports_0)} alt="Step 1 : kelas bahasa mandarin internal iik bhakta"><img${ssrRenderAttr("src", _imports_1)} alt="Step 2 : kelas bahasa mandarin internal iik bhakta"><img${ssrRenderAttr("src", _imports_2)} alt="Step 3 : kelas bahasa mandarin internal iik bhakta"></div><div class="md:w-2/3 lg:w-4/5 px-2 md:px-0"><ol class="list-decimal list-outside space-y-2"><li>Mahasiswa Log in akun OASIS, pilih menu Kelas Bahasa.</li><li>Pilih kelas Mandarin, klik add.</li><li>Tahun Ajaran dan Semester diisi sesuai dengan tahun akademik dan semester yang berlangsung, Tahap diisi level kelas Mandarin yang ditempuh (level 1 atau level 2), kemudian pilin Kelas yang dikehendaki (pastikan tidak kres dengan dalam kelas yang dituju sudah penuh, maka mahasiswa memilih jadwal yang lain.)</li><li>Dalam satu rombel, dibatasi 35-40 kuota mahasiswa lintas prod. Apabila kuota dalam kelas yang dituju sudah penuh, maka mahasiswa memilih jadwal yang lain.</li><li> Mahasiswa diperbolehkan mengajukan request pindah kelas di OASIS apabila ternyata bentrok dengan jadwal perkuliahan yang lain. Mahasiswa harus mengkonfirmasi pengajuan pindah kelas ini ke Telegram <a class="text-sky-600 font-bold" href="https://t.me/tanyabahasa">@tanyabahasa</a> untuk mendapat tindaklanjut, ajuan disetujui atau ditolak. Request pindah kelas dapat diajukan sebelum batas akhir pendaftaran kelas bahasa dengan mempertimbangkan kapasitas kelas yang dituju dan alasan </li><li> Apabila mahasiswa mengalami kendala atau pertanyaan saat mendaftar, bisa menghubungi Telegram <a class="text-sky-600 font-bold" href="https://t.me/tanyabahasa">@tanyabahasa</a> sebelum batas akhir pendaftaran. </li></ol></div></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/programs/kelas-bahasa-mandarin/pendaftaran.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const pendaftaran = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { pendaftaran as default };
//# sourceMappingURL=pendaftaran-34bfc5fa.mjs.map
