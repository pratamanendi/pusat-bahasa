import { mergeProps, useSSRContext } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/index.mjs';
import { ssrRenderAttrs } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/server-renderer/index.mjs';
import { _ as _export_sfc } from './server.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ofetch@1.0.1/node_modules/ofetch/dist/node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/hookable@5.5.1/node_modules/hookable/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unctx@2.1.2/node_modules/unctx/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+ssr@1.1.23/node_modules/@unhead/ssr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unhead@1.1.23/node_modules/unhead/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+shared@1.1.23/node_modules/@unhead/shared/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-router@4.1.6_vue@3.2.47/node_modules/vue-router/dist/vue-router.node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/h3@1.6.2/node_modules/h3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ufo@1.1.1/node_modules/ufo/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/defu@6.1.2/node_modules/defu/dist/defu.mjs';
import './nitro-prerenderer.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/node-fetch-native@1.0.2/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/destr@1.2.2/node_modules/destr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unenv@1.2.2/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ohash@1.0.0/node_modules/ohash/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/drivers/fs.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/radix3@1.0.0/node_modules/radix3/dist/index.mjs';

const _sfc_main = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "space-y-3 lg:min-h-[40vh] text-sm" }, _attrs))} data-v-f65fcdc0><h1 class="font-bold capitalize text-base" data-v-f65fcdc0>sasaran peserta</h1><p class="text-justify" data-v-f65fcdc0>Program kelas bahasa Mandarin bersifat wajib bagi seluruh mahasiswa IIK Bhakta Kediri Program studi D3, D4 dan S1. Kelas bahasa Mandarin terdiri dai 2 level/tahap dengan informasi sebagai berikut: </p><ol class="list-decimal list-outside !mt-0" data-v-f65fcdc0><li data-v-f65fcdc0>Mahasiswa Program Studi D3, D4, dan S1 program reguler dan progsus bisa mengambil kelas bahasa Mandarin level 1 di semester 2 dan melanjutkan mengambil level 2 di semester berikutnya apabila sudah lulus level 1. Level 1 menjadi prasyarat mahasiswa untuk bisa mengambil level 2.</li><li data-v-f65fcdc0>Mahasiswa Program RPL mengambil kelas bahasa Mandarin di semester 1 dan melanjutkan mengambil level 2 di semester berikutnya apabilsa sudah lulus level 1. Level 1 menjadi prasyarat mahasiswa untuk bisa mengambil level 2.</li><li data-v-f65fcdc0>Apabila mahasiswa tidak lulus level 1 atau level 2, maka mahasiswa mengulang mengambil kelas bahasa Mandarin di level tersebut pada semester selanjutnya.</li></ol></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/programs/kelas-bahasa-mandarin/sasaran-peserta.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const sasaranPeserta = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender], ["__scopeId", "data-v-f65fcdc0"]]);

export { sasaranPeserta as default };
//# sourceMappingURL=sasaran-peserta-b460c3f4.mjs.map
