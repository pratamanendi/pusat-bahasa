import { mergeProps, useSSRContext } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/index.mjs';
import { ssrRenderAttrs } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/server-renderer/index.mjs';
import { _ as _export_sfc } from './server.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ofetch@1.0.1/node_modules/ofetch/dist/node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/hookable@5.5.1/node_modules/hookable/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unctx@2.1.2/node_modules/unctx/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+ssr@1.1.23/node_modules/@unhead/ssr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unhead@1.1.23/node_modules/unhead/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+shared@1.1.23/node_modules/@unhead/shared/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-router@4.1.6_vue@3.2.47/node_modules/vue-router/dist/vue-router.node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/h3@1.6.2/node_modules/h3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ufo@1.1.1/node_modules/ufo/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/defu@6.1.2/node_modules/defu/dist/defu.mjs';
import './nitro-prerenderer.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/node-fetch-native@1.0.2/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/destr@1.2.2/node_modules/destr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unenv@1.2.2/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ohash@1.0.0/node_modules/ohash/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/drivers/fs.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/radix3@1.0.0/node_modules/radix3/dist/index.mjs';

const _sfc_main = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "space-y-3 lg:min-h-[40vh] text-sm" }, _attrs))} data-v-379490b9><h1 class="font-bold capitalize text-base" data-v-379490b9>kelas bahasa inggris umum</h1><p data-v-379490b9> Selain memfasilitasi kelas Bahasa Inggris untuk internal, Pusat Bahasa IIK Bhakta juga membuka kelas bahasa untuk umum yaitu \u201CTOEIC Preparation\u201D. Kelas umum ini dilaksanakan kurang lebih dua bulan dan pembelajarannya dilakukan secara online. Jumlah pertemuan dalam kelas ini sebanyak 14x dengan pertemuan rutin 2x per minggunya. Durasi pembelajaran 90 menit dan didampingi oleh tim pengajar Pusat Bahasa yang kompeten dibidangnya. <br data-v-379490b9> Setelah mengikuti kelas bahasa ini, peserta diharapkan langsung mengikuti tes TOEIC yang bekerjasama langsung antara Pusat Bahasa IIK Bhakta dengan ITC (International Test Center). Tes TOEIC akan dilaksanakan dengan metode Paper Based Test (PBT) di kampus IIK Bhakta. Peserta juga akan mendapat sertifikat TOEIC yang berlisensi resmi dari ITC. </p><p class="font-bold" data-v-379490b9> Fasilitas yang diperoleh peserta: </p><ol class="!mt-0 list-decimal list-outside" data-v-379490b9><li data-v-379490b9>Pembelajaran disesuaikan dengan materi TOEIC dari ITC yang sudah berstandar internasional.</li><li data-v-379490b9>Untuk meningkatkan skill, kami menyediakan buku modul khusus TOEIC dari ITC yang dapat dibeli di Bhakta Shop senilai Rp 200.000.</li><li data-v-379490b9>Praktis dan mudah karena pembelajaran dilakukan secara online.</li><li data-v-379490b9>Tutor komunikatif dan kompeten dibidangnya.</li><li data-v-379490b9>Tips dan Trik mengerjakan soal TOEIC.</li></ol><p class="font-bold" data-v-379490b9> NB: </p><ol class="!mt-0 list-decimal list-outside" data-v-379490b9><li data-v-379490b9>Kelas akan dibuka dengan syarat minimal terisi 5 peserta.</li><li data-v-379490b9>Juga tersedia kelas privat dengan waktu pembelajaran yang dapat dipilih antara hari Senin-Jumat pukul 07:00-15:00 WIB</li><li data-v-379490b9>Pendaftaran maksimal H-3 sebelum tanggal dimulainya kelas</li></ol><div class="relative overflow-x-auto md:rounded-2xl rounded-lg border border-[#0d467d]" data-v-379490b9><table class="hidden w-full text-sm text-center" data-v-379490b9><thead class="" data-v-379490b9><tr class="bg-[#0d467d] divide-x divide-white" data-v-379490b9><th scope="col" class="px-6 py-1.5" data-v-379490b9> Biaya Kelas &quot;TOEIC Preparation Reguler&quot; </th><th scope="col" class="px-6 py-1.5" data-v-379490b9> Biaya privat &quot;TOEIC Preparation&quot; </th><th scope="col" class="px-6 py-1.5" data-v-379490b9> Biaya tes TOEIC </th></tr></thead><tbody data-v-379490b9><tr class="" data-v-379490b9><td class="px-6 py-2" data-v-379490b9> Rp. 425.000 </td><td class="px-6 py-2 bg-indigo-100" data-v-379490b9> Rp. 700.000 </td><td class="px-6 py-2" data-v-379490b9> Rp. 675.000 </td></tr></tbody></table><table id="tbl-mbl" class="w-full text-sm text-left" data-v-379490b9><tbody class="divide-y divide-white" data-v-379490b9><tr data-v-379490b9><th class="w-1/2 bg-[#0d467d]" data-v-379490b9>Biaya Kelas &quot;TOEIC Preparation Reguler&quot;</th><td data-v-379490b9>Rp. 425.000</td></tr><tr data-v-379490b9><th class="bg-[#0d467d]" data-v-379490b9>Biaya Privat &quot;TOEIC Preparation&quot;</th><td class="border-y border-[#0d467d] bg-sky-100" data-v-379490b9>Rp. 700.000</td></tr><tr data-v-379490b9><th class="bg-[#0d467d]" data-v-379490b9>Biaya tes TOEIC</th><td data-v-379490b9>Rp 675.000</td></tr></tbody></table></div><div class="md:flex gap-4 items-center md:py-0 py-4" data-v-379490b9><a href="" class="bg-[#0d467d] font-semibold text-white rounded-md px-2 py-1.5" data-v-379490b9>Daftar Sekarang</a><p class="font-bold md:mt-0 mt-4" data-v-379490b9>(Periode 16 Januari 2023 - 04 Maret 2023)</p></div><p data-v-379490b9>Ayo tingkatkan skill bahasa Inggris Anda sekarag juga. Bersama Pusat Bahasa IIK Bhakta, belajar bahasa Inggris jadi lebih mudah dan menyenangkan.</p></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/programs/kelas-bahasa-inggris/umum.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const umum = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender], ["__scopeId", "data-v-379490b9"]]);

export { umum as default };
//# sourceMappingURL=umum-5b00659a.mjs.map
