const default_vue_vue_type_style_index_0_scoped_9929f190_lang = ".footer-item[data-v-9929f190]{align-items:center;gap:.75rem;justify-content:flex-start}.footer-item[data-v-9929f190]>:not([hidden])~:not([hidden]){--tw-divide-x-reverse:0;border-left-width:2px;border-left-width:calc(2px*(1 - var(--tw-divide-x-reverse)));border-right-width:0;border-right-width:calc(2px*var(--tw-divide-x-reverse))}.footer-item>img[data-v-9929f190]{width:5%}.footer-item[data-v-9929f190]>:not(img){font-size:.875rem;font-weight:100;line-height:1.25rem;padding-left:.75rem}";

const defaultStyles_220b39e0 = [default_vue_vue_type_style_index_0_scoped_9929f190_lang];

export { defaultStyles_220b39e0 as default };
//# sourceMappingURL=default-styles.220b39e0.mjs.map
