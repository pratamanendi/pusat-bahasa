const interopDefault = r => r.default || r || [];
const styles = {
  "pages/profile/visimisi.vue": () => import('./visimisi-styles.6483946f.mjs').then(interopDefault),
  "pages/programs/english-for-mapping/pelaksanaan.vue": () => import('./pelaksanaan-styles.de61f433.mjs').then(interopDefault),
  "pages/programs/kelas-bahasa-inggris/sasaran-peserta.vue": () => import('./sasaran-peserta-styles.841472c4.mjs').then(interopDefault),
  "pages/programs/kelas-bahasa-inggris/umum.vue": () => import('./umum-styles.cfcefce2.mjs').then(interopDefault),
  "pages/programs/kelas-bahasa-mandarin/sasaran-peserta.vue": () => import('./sasaran-peserta-styles.6f280ece.mjs').then(interopDefault),
  "pages/programs/program-toeic-public.vue": () => import('./program-toeic-public-styles.082553c5.mjs').then(interopDefault),
  "pages/programs/program-toeic/pelaksanaan.vue": () => import('./pelaksanaan-styles.1a070728.mjs').then(interopDefault),
  "pages/programs/program-toeic/umum.vue": () => import('./umum-styles.aa41854a.mjs').then(interopDefault),
  "pages/programs/program-toeic.vue": () => import('./program-toeic-styles.9dee1552.mjs').then(interopDefault),
  "app.vue": () => import('./app-styles.d7d2023b.mjs').then(interopDefault),
  "node_modules/.pnpm/@nuxt+ui-templates@1.1.1/node_modules/@nuxt/ui-templates/dist/templates/error-404.vue": () => import('./error-404-styles.f4b6ac90.mjs').then(interopDefault),
  "node_modules/.pnpm/@nuxt+ui-templates@1.1.1/node_modules/@nuxt/ui-templates/dist/templates/error-500.vue": () => import('./error-500-styles.af9750fa.mjs').then(interopDefault),
  "layouts/default.vue": () => import('./default-styles.220b39e0.mjs').then(interopDefault),
  "components/Navbar.vue": () => import('./Navbar-styles.d113f11d.mjs').then(interopDefault)
};

export { styles as default };
//# sourceMappingURL=styles.mjs.map
