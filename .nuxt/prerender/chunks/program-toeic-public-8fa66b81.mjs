import { p as publicAssetsURL } from './renderer.mjs';
import { mergeProps, useSSRContext } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/index.mjs';
import { ssrRenderAttrs, ssrRenderAttr } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/server-renderer/index.mjs';
import { _ as _export_sfc, u as useHead } from './server.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-bundle-renderer@1.0.2/node_modules/vue-bundle-renderer/dist/runtime.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/h3@1.6.2/node_modules/h3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ufo@1.1.1/node_modules/ufo/dist/index.mjs';
import './nitro-prerenderer.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/node-fetch-native@1.0.2/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ofetch@1.0.1/node_modules/ofetch/dist/node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/destr@1.2.2/node_modules/destr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unenv@1.2.2/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/hookable@5.5.1/node_modules/hookable/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/defu@6.1.2/node_modules/defu/dist/defu.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ohash@1.0.0/node_modules/ohash/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/drivers/fs.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/radix3@1.0.0/node_modules/radix3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unctx@2.1.2/node_modules/unctx/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+ssr@1.1.23/node_modules/@unhead/ssr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unhead@1.1.23/node_modules/unhead/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+shared@1.1.23/node_modules/@unhead/shared/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-router@4.1.6_vue@3.2.47/node_modules/vue-router/dist/vue-router.node.mjs';

const _imports_0 = "" + publicAssetsURL("img/asset-21.webp");
const _sfc_main = {
  __name: "program-toeic-public",
  __ssrInlineRender: true,
  setup(__props) {
    const title = "Program TOEIC Public Pusat Bahasa IIK Bhakti Wiyata";
    useHead({
      title,
      meta: [
        { name: "description", content: title }
      ]
    });
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "w-full lg:min-h-screen bg-slate-200 bg-gradient-to-tr from-red-400/30 to-indigo-400/30 md:px-8 md:pt-8 p-4" }, _attrs))} data-v-e415efcc><h1 class="md:text-2xl text-xl font-bold capitalize mb-4 md:px-16" data-v-e415efcc>Program TOEIC Public (Eksternal)</h1><div class="md:flex" data-v-e415efcc><div class="md:w-1/2 md:relative" data-v-e415efcc><img${ssrRenderAttr("src", _imports_0)} alt="Program TOEIC Public (Eksternal)" class="md:absolute" data-v-e415efcc></div><div class="space-y-4 md:w-1/2 text-sm" data-v-e415efcc><p data-v-e415efcc>IIK Bhakti Wiyata bekerja sama dengan International Test Center (ITC) dalam mengadakan tes TOEIC Public.</p><p class="font-bold" data-v-e415efcc>Ketentuan TOEIC Public di IIK Bhakti Wiyata</p><ol data-v-e415efcc><li data-v-e415efcc>Tes TOEIC yang disediakan adalah PBT (Paper Based Test).</li><li data-v-e415efcc>Tes dilaksanakan pada pagi hari 10:00-12:30 WIB di A508 (CBT 1) Lantai 5, Gedung Adipadma.</li><li data-v-e415efcc> Pendaftaran melalui website ITC <a href="http://www.itc-indonesia.co.id/registration/login.php" target="_blank" class="text-sky-600 font-bold" rel="noopener noreferrer" data-v-e415efcc> http://www.itc-indonesia.co.id/registration/login.php </a> Pendaftaran ditutup oleh sistem ITC H-7 menjelang pelaksanaan tes.</li><li data-v-e415efcc>Biaya tertera di Website ITC yakni 675.000 (tidak termasuk biaya pengiriman hasil tes)</li><li data-v-e415efcc>Pilih Lokasi \u201CIIK Bhakti Wiyata Kediri&quot;</li><li data-v-e415efcc>Lakukan Pembayaran dengan nominal tertera</li><li data-v-e415efcc>Tidak ada sistem refund, bagi peserta yang tidak mengikuti tes TOEIC PBT.</li><li data-v-e415efcc>Hasil tes dikirimkan ke alamat peserta via jasa ekspedisi, seminggu setelah pelaksanaan tes.</li></ol><p class="font-bold" data-v-e415efcc>Syarat Mengikuti Tes TOEIC Public di IIK Bhakti Wiyata</p><ol data-v-e415efcc><li data-v-e415efcc>Membawa bukti pendaftaran, KTP atau tanda pengenal lain (SIM atau Paspor)</li><li data-v-e415efcc>Membawa kartu vaksin atau terdaftar di PeduliLindungi (Minimal vaksin dosis kedua)</li><li data-v-e415efcc>Memakai masker dan mempersiapkan keperluan pribadi yang diperlukan (handsanitizer, dll)</li><li data-v-e415efcc>Membawa perlengkapan alat tulis (Pensil 2B merek bebas, asalkan bukan pensil mekanik, penghapus, rautan, dan, pen)</li><li data-v-e415efcc>Tidak diperkenankan membawa makanan dan minuman ke dalam ruangan.</li></ol><p data-v-e415efcc><small data-v-e415efcc>*</small> Peserta akan dihubungi oleh perwakilan Pusat Bahasa IIK Bhakti Wiyata H-5 pelaksanaan tes Narahubung TOEIC Public IIK Bhakti Wiyata <a href="http://wa.me/6285706577707" data-v-e415efcc>085706577707</a> (WhatsApp) </p><p class="!mt-0" data-v-e415efcc><small data-v-e415efcc>*</small> Jadwal TOEIC Listening and Reading (Public Test) dapat dilihat di <a class="font-bold text-sky-600" href="https://itc-indonesia.com/jadwal-tes/" data-v-e415efcc>https://itc-indonesia.com/jadwal-tes/</a></p></div></div></div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/programs/program-toeic-public.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const programToeicPublic = /* @__PURE__ */ _export_sfc(_sfc_main, [["__scopeId", "data-v-e415efcc"]]);

export { programToeicPublic as default };
//# sourceMappingURL=program-toeic-public-8fa66b81.mjs.map
