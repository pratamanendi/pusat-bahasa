import { mergeProps, useSSRContext } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/index.mjs';
import { ssrRenderAttrs } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/server-renderer/index.mjs';
import { _ as _export_sfc } from './server.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ofetch@1.0.1/node_modules/ofetch/dist/node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/hookable@5.5.1/node_modules/hookable/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unctx@2.1.2/node_modules/unctx/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+ssr@1.1.23/node_modules/@unhead/ssr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unhead@1.1.23/node_modules/unhead/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+shared@1.1.23/node_modules/@unhead/shared/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-router@4.1.6_vue@3.2.47/node_modules/vue-router/dist/vue-router.node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/h3@1.6.2/node_modules/h3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ufo@1.1.1/node_modules/ufo/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/defu@6.1.2/node_modules/defu/dist/defu.mjs';
import './nitro-prerenderer.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/node-fetch-native@1.0.2/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/destr@1.2.2/node_modules/destr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unenv@1.2.2/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ohash@1.0.0/node_modules/ohash/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/drivers/fs.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/radix3@1.0.0/node_modules/radix3/dist/index.mjs';

const _sfc_main = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "space-y-3 lg:min-h-[40vh] text-sm" }, _attrs))} data-v-5bead7a5><h1 class="font-bold capitalize text-base" data-v-5bead7a5>sasaran peserta</h1><p class="text-justify" data-v-5bead7a5>Program kelas bahasa Inggris merupakan program persiapan TOEIC yang wajib ditempuh oleh mahasiswa IIK Bhakta Kediri. Program ini berlaku bagi mahasiswa prodi D3, D4 dan S1 program reguler dan non regular, dengan ketentuan sebagai berikut: </p><ol class="list-decimal list-outside !mt-0" data-v-5bead7a5><li data-v-5bead7a5>Mahasiswa prodi D3 (program reguler dan progsus) bisa mengambil kelas ini mulai semester 3.</li><li data-v-5bead7a5>Mahasiswa prodi D4/S1 (program reguler) bisa mengambil kelas ini mulai semester 5.</li><li data-v-5bead7a5>Mahasiswa prodi D4/S1 (program non-reguler RPL) wajib mengambil kelas ini pada semester 1.</li></ol><p data-v-5bead7a5> Mahasiswa wajib mengikuti tes English for Mapping terlebih dulu sebagai prasyarat mendaftar dan mengikuti kelas bahasa Inggris. Penjelasan mengenai tes English for Mapping ini bisa dilihat pada Panduan Tes English for Mapping. </p></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/programs/kelas-bahasa-inggris/sasaran-peserta.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const sasaranPeserta = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender], ["__scopeId", "data-v-5bead7a5"]]);

export { sasaranPeserta as default };
//# sourceMappingURL=sasaran-peserta-e3777826.mjs.map
