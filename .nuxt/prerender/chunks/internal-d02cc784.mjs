import { p as publicAssetsURL } from './renderer.mjs';
import { ssrRenderAttrs, ssrRenderAttr } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/server-renderer/index.mjs';
import { _ as _imports_0, a as _imports_2 } from './asset-24-4d3533db.mjs';
import { useSSRContext } from 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue@3.2.47/node_modules/vue/index.mjs';
import { _ as _export_sfc } from './server.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-bundle-renderer@1.0.2/node_modules/vue-bundle-renderer/dist/runtime.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/h3@1.6.2/node_modules/h3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ufo@1.1.1/node_modules/ufo/dist/index.mjs';
import './nitro-prerenderer.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/node-fetch-native@1.0.2/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ofetch@1.0.1/node_modules/ofetch/dist/node.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/destr@1.2.2/node_modules/destr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unenv@1.2.2/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/hookable@5.5.1/node_modules/hookable/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/defu@6.1.2/node_modules/defu/dist/defu.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/ohash@1.0.0/node_modules/ohash/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unstorage@1.4.1/node_modules/unstorage/drivers/fs.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/radix3@1.0.0/node_modules/radix3/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unctx@2.1.2/node_modules/unctx/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+ssr@1.1.23/node_modules/@unhead/ssr/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/unhead@1.1.23/node_modules/unhead/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/@unhead+shared@1.1.23/node_modules/@unhead/shared/dist/index.mjs';
import 'file://C:/Users/IT-PC/Documents/pusat-bahasa/node_modules/.pnpm/vue-router@4.1.6_vue@3.2.47/node_modules/vue-router/dist/vue-router.node.mjs';

const _imports_1 = "" + publicAssetsURL("img/asset-26.webp");
const _sfc_main = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  _push(`<div${ssrRenderAttrs(_attrs)}><h1 class="font-bold capitalize text-base mb-3">kelas bahasa inggris internal</h1><div class="md:flex gap-8 text-sm lg:min-h-[40vh]"><div class="lg:w-1/5 md:w-1/3 space-y-6 py-4 md:block hidden"><img${ssrRenderAttr("src", _imports_0)} alt="Step 1 : kelas bahasa inggris internal iik bhakta"><img${ssrRenderAttr("src", _imports_1)} alt="Step 2 : kelas bahasa inggris internal iik bhakta"><img${ssrRenderAttr("src", _imports_2)} alt="Step 3 : kelas bahasa inggris internal iik bhakta"></div><div class="lg:w-4/5 md:w-2/3"><ol class="list-decimal list-outside space-y-2 px-4 md:px-0"><li>Log in akun OASIS, pilih menu Kelas Bahasa.</li><li>Pilih kelas English, klik add.</li><li>Tahun Ajaran dan Semester diisi sesuai dengan tahun akademik dan semester yang berlangsung, Tahap diisi 1 (arena kelas bahasa Inggris hanya 1 tahap/level), kemudian pilih Kelas yang dikehendaki. Pastikan tidak bentrok dengan jadwal perkuliahan lainnya, lalu ik Simpan.</li><li>Dalam satu rombel, dibatasi 35-40 kuota mahasiswa lintas prod. Apabila kuota dalam kelas yang dituju sudah penuh, maka mahasiswa memilih jadwal yang lain.</li><li>Mahasiswa diperbolehkan mengajukan request pindah kelas di OASIS apabila ternyata bentrok dengan jadwal perkuliahan yang lain. Mahasiswa harus mengkonfirmasi penga juan pindah kelas ini ke Telegram <a class="text-indigo-700 font-semibold" href="https://t.me/tanyabahasa">@tanyabahasa</a> untuk mendapat tindaklanjut, ajuan disetujui atau ditolak. Request pindah kelas dapat diajukan sebelum batas akhir pendaftaran kelas bahasa dengan mempertimbangkan kapasitas kelas yang dituju dan alasan pindah kelas.</li><li>Apabila mahasiswa mengalami kendala atau pertanyaan saat mendaftar, bisa menghubungi Telegram <a class="text-indigo-700 font-semibold" href="https://t.me/tanyabahasa">@tanyabahasa</a> sebelum batas akhir pendaftaran.</li></ol></div></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/programs/kelas-bahasa-inggris/internal.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const internal = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { internal as default };
//# sourceMappingURL=internal-d02cc784.mjs.map
